import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faChartBar } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faLayerGroup } from '@fortawesome/free-solid-svg-icons'
import { faBook } from '@fortawesome/free-solid-svg-icons'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons'
import { faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons'
import {faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import "chart.js"
import "hchs-vue-charts"
import Vue from 'vue'
import Dashboard from './dashboard.vue'



library.add(faHome)
library.add(faLayerGroup)
library.add(faChartBar)
library.add(faUser)
library.add(faBook)
library.add(faCog)
library.add(faEdit)
library.add(faTrashAlt)
library.add(faArrowAltCircleLeft)
library.add(faArrowAltCircleRight)
library.add(faSearch)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(window.VueCharts);
Vue.config.productionTip = false

new Vue({  render: h => h(Dashboard,),}).$mount('#app')
